// ############# How to set item in the localStorage ####################
// ----------------------------------------------------------------------

/* localStorage.setItem("name", "sandeep");
localStorage.setItem("greet", "Good morning"); */




// ############## How to set item in the sessionStorage #################
// ----------------------------------------------------------------------

/* sessionStorage.setItem("name", "sandeep");
sessionStorage.setItem("greet", "Good morning"); */




// ################# Length of the localStorage ######################
// -------------------------------------------------------------------

/* console.log("Length of LocalStorage :", (localStorage.length)); */




// ################ Length of the sessionStorage #######################
// ---------------------------------------------------------------------

/* console.log("Length of SessionStorage :", (sessionStorage.length)); */




// ############## How to get item from the localStorage ################
// ---------------------------------------------------------------------

/* let name = localStorage.getItem("name");
let greet = localStorage.getItem("greet");
console.log(`${greet}, My name is ${name}.`); */




// ############ How to get item from the sessionStorage #################
// ----------------------------------------------------------------------

/* let name = sessionStorage.getItem("name");
let greet = sessionStorage.getItem("greet");
console.log(`${greet}, My name is ${name}.`); */





// ############ How to remove item from the localStorage ################
// ----------------------------------------------------------------------

/* localStorage.removeItem("name"); */




// ############ How to remove item from the sessionStorage ##############
// ----------------------------------------------------------------------

/* sessionStorage.removeItem("name"); */




// ############### How to clear the localStorage #####################
// -------------------------------------------------------------------

/* localStorage.clear(); */




// ############## How to clear the sessionStorage #####################
// --------------------------------------------------------------------

/* sessionStorage.clear(); */




// ############# How to push an array in localStorage ###################
// ----------------------------------------------------------------------

/* let fruits = ["Apple", "Banana", "Coconut", "Date"];
localStorage.setItem("fruits", JSON.stringify(fruits)); */




// ########### How to push an array in sessionStorage ################
// -------------------------------------------------------------------

/* let fruits = ["Apple", "Banana", "Coconut", "Date"];
sessionStorage.setItem("fruits", JSON.stringify(fruits)); */




// ######## How to get an array from the localStorage ##############
// -----------------------------------------------------------------

/* let fruits = JSON.parse(localStorage.getItem("fruits"));
console.log("Fruits :", fruits); */




// ########### How to get an array from the sessionStorage ############
// --------------------------------------------------------------------

/* let fruits = JSON.parse(sessionStorage.getItem("fruits"));
console.log("Fruits :", fruits); */

