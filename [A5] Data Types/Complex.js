// Function
let greeting = () => "Good Morning";
console.log("Data :", greeting, "| Data Type : ", typeof greeting);

// Object
let fruits = ["Apple", "Banana", "Coconut", "Date", "Grapes"];
let student = { name: "Sandeep", age: 21 };
let income = null;
let date = new Date();

console.log("Data :", fruits, "| Data Type : ", typeof fruits);
console.log("Data :", student, "| Data Type:", typeof student);
console.log("Data :", income, "| Data Type :", typeof income);
console.log("Data :", date, "| Data Type :", typeof date);