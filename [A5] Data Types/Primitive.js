// String
let nameOfCat = "Pussy";
console.log("Data :", nameOfCat, "| Data Type :", typeof nameOfCat);

// Number
let ageOfCat = 10;
console.log("Data :", ageOfCat, "| Data Type :", typeof ageOfCat);

// Boolean
let isAlive = true;
console.log("Data :", isAlive, "| Data Type :", typeof isAlive);

// Undefined
let bugs = undefined;
console.log("Data :", bugs, "| Data Type :", typeof bugs);