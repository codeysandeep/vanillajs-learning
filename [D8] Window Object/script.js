// ####################### closed #####################
// ---------------------------------------------------

/* let element = window.closed;
console.log(`Window is closed? : ${element}`); */




// ################### document #####################
// --------------------------------------------------

/* let element = window.document;
console.log(element); */




// ################### history ###################
// -----------------------------------------------

/* let element = window.history;
console.log(element); */




// #################### innerHeight ###################
// ---------------------------------------------------

/* let element = window.innerHeight;
console.log(`Inner Height : ${element}`); */




// ################### innerWidth #################
// ------------------------------------------------

/* let element = window.innerWidth;
console.log(`Inner Width : ${element}`); */




// ################## localStorage ###################
// ---------------------------------------------------

/* localStorage.setItem("name", "sandeep");
console.log(`My name is ${localStorage.getItem("name")}.`); */




// #################### location #####################
// ---------------------------------------------------

/* let element = window.location;
console.log(element); */




// ################# outerHeight ####################
// ---------------------------------------------------

/* let element = window.outerHeight;
console.log(`Outer Height : ${element}`); */




// ################# outerWidth ##################
// ----------------------------------------------

/* let element = window.outerWidth;
console.log(`Outer Width : ${element}`); */




// ################## pageXOffset ##################
// --------------------------------------------------

/* window.scrollBy(100, 100);
console.log(pageXOffset); */




// ################ pageYOffset ##################
// -----------------------------------------------

/* window.scrollBy(100, 100);
console.log(pageYOffset); */




// #################### screen ###################
// -----------------------------------------------

/* let element = window.screen;
console.log(element); */




// ################### sessionStorage #################
// ----------------------------------------------------

/* sessionStorage.setItem("name", "sandeep");
console.log(`My name is ${sessionStorage.getItem("name")}.`); */




// ################ status ####################
// ---------------------------------------------

/* window.status = "Some text in the status bar.";
let element = window.status;
console.log(element); */

