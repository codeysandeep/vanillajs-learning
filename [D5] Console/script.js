// ########################## console.assert() #####################
// -----------------------------------------------------------------

/* console.assert(1==1, "Condition is True!");
console.assert(1!=1, "Condition is False!"); */




// ################### console.clear() ####################
// --------------------------------------------------------

/* console.log("Please clear your console!");
console.clear(); */




// ##################### console.count() ##################
// --------------------------------------------------------

/* for(let i=1; i<=10; i++) {
    console.count("Count");
}; */




// ##################### console.countReset() ##################
// --------------------------------------------------------------

/* for(let i=1; i<=10; i++) {
    console.count("Count");
    if(i===5){
        console.countReset("Count");
    };
}; */




// ########################### console.error() #########################
// ---------------------------------------------------------------------

/* console.error("Error: You made a mistake!"); */




// ###################### console.group() ##############################
// ---------------------------------------------------------------------

/* console.group("Top seven fruits of the world :");
console.log("Apple");
console.log("Banana");
console.log("Coconut");
console.log("Date");
console.log("Grapes");
console.log("Mango");
console.log("Papaya"); */




// ######################## console.groupCollapsed() ###################
// ---------------------------------------------------------------------

/* console.groupCollapsed("Top seven fruits of the world :");
console.log("Apple");
console.log("Banana");
console.log("Coconut");
console.log("Date");
console.log("Grapes");
console.log("Mango");
console.log("Papaya"); */




// ######################## console.groupEnd() ######################
// ------------------------------------------------------------------

/* console.group("Top seven fruits of the world :");
console.log("Apple");
console.log("Banana");
console.log("Coconut");
console.log("Date");
console.log("Grapes");
console.log("Mango");
console.log("Papaya");
console.groupEnd(); */




// ########################## console.info() ##########################
// -------------------------------------------------------------------

/* console.info("Information: VanillaJS is easy to learn!"); */




// ####################### console.log() #########################
// ---------------------------------------------------------------

/* console.log("Hello VanillaJS, I am ready to learn you!"); */




// #################### console.table() #####################
// ----------------------------------------------------------

/* var fruits = [
    {
        name: "Apple",
        price: 10
    },
    {
        name: "Banana",
        price: 20
    },
    {
        name: "Coconut",
        price: 30
    },
    {
        name: "Date",
        price: 40
    },
    {
        name: "Grapes",
        price: 50
    }
];
console.table(fruits); */




// ######################### console.time() #####################
// --------------------------------------------------------------

/* console.time("Total Taken Time");
for (let i = 1; i <= 100; i++) {
    console.log(i);
};
console.timeEnd("Total Taken Time"); */




// ######################## console.timeEnd() ####################
// ---------------------------------------------------------------

/* console.time("Total Taken Time");
for (let i = 1; i <= 100; i++) {
    console.log(i);
};
console.timeEnd("Total Taken Time"); */




// ###################### console.timeLog() #####################
// --------------------------------------------------------------

/* console.time("Log Time");
for (let i = 1; i <= 10; i++) {
    console.timeLog("Log Time");
}; */




// ##################### console.timeStamp() ######################
// ----------------------------------------------------------------

/* console.time("Time Stamp");
for (let i = 1; i <= 100; i++) {
    console.timeStamp("Time Stamp");
};
console.timeEnd("Time Stamp"); */




// ##################### console.trace() ##########################
// ----------------------------------------------------------------

/* function parentFunction() {
    firstFunction();
};
function firstFunction() {
    secondFunction();
};
function secondFunction() {
    console.trace("This is, How can we Trace?");
};
parentFunction(); */




// ######################## console.warn() #########################
// -----------------------------------------------------------------

/* console.warn("Warning: Do not Repeat yourself!"); */
