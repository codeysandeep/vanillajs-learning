const dictionary = {
    A: "Apple",
    B: "Banana",
    C: "Coconut",
    D: "Dog",
    E: "Elephant",
    F: "Fish",
    G: "Grapes",
    H: "Hen",
    I: "Ink",
    J: "Jungle",
    K: "King",
    L: "Lion",
    M: "Mango",
    N: "Nose",
    O: "Orange",
    P: "Papaya",
    Q: "Queen",
    R: "Rose",
    S: "Sun",
    T: "Tree",
    U: "Umbrella",
    V: "Vampire",
    W: "Watch",
    X: "X-Ray",
    Y: "Yellow",
    Z: "Zebra",
};

console.log(`A for ${dictionary.A}`);
console.log(`B for ${dictionary.B}`);
console.log(`C for ${dictionary.C}`);
console.log(`D for ${dictionary.D}`);
console.log(`E for ${dictionary.E}`);
console.log(`F for ${dictionary.F}`);
console.log(`G for ${dictionary.G}`);
console.log(`H for ${dictionary.H}`);
console.log(`I for ${dictionary.I}`);
console.log(`J for ${dictionary.J}`);
console.log(`K for ${dictionary.K}`);
console.log(`L for ${dictionary.L}`);
console.log(`M for ${dictionary.M}`);
console.log(`N for ${dictionary.N}`);
console.log(`O for ${dictionary.O}`);
console.log(`P for ${dictionary.P}`);
console.log(`Q for ${dictionary.Q}`);
console.log(`R for ${dictionary.R}`);
console.log(`S for ${dictionary.S}`);
console.log(`T for ${dictionary.T}`);
console.log(`U for ${dictionary.U}`);
console.log(`V for ${dictionary.V}`);
console.log(`W for ${dictionary.W}`);
console.log(`X for ${dictionary.X}`);
console.log(`Y for ${dictionary.Y}`);
console.log(`Z for ${dictionary.Z}`);