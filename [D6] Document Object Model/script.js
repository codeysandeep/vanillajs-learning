// ####################### addEventListener() ########################
// ---------------------------------------------------------------

/* let innerBox = document.querySelector(".inner-container");
innerBox.addEventListener("click", function(){
    console.log("Message from Inner Box");
}, false);

let outerBox = document.querySelector(".outer-container");
outerBox.addEventListener("click", function(){
    console.log("Message from Outer Box!");
}, true); */




// ################# appendMethods "appendChild()" ##################
// -------------------------------------------------------------------

/* let paragraph = document.createElement("p");
let text = document.createTextNode("Sandeep Malothiya");
paragraph.appendChild(text);
document.getElementById("paragraph-container").appendChild(paragraph); */




// ############### appendMethods "insertBefore()" #############
// -----------------------------------------------------------

/* let text = document.createTextNode("Sandeep Malothiya");
let paragraphContainer = document.getElementById("paragraph-container");
paragraphContainer.insertBefore(text, paragraphContainer.childNodes[0]); */




// ############# appendMethods "insertAdjacentElement()" ############
// ------------------------------------------------------------------

/* let paragraph = document.createElement("p");
let text = document.createTextNode("Sandeep Malothiya");
paragraph.appendChild(text);
document.getElementById("paragraph-container").insertAdjacentElement("afterbegin", paragraph); */




// ########## appendMethods "insertAdjacentHTML()" ##############
// ---------------------------------------------------------------

/* let paragraph = "<p>Sandeep Malothiya</p>";
document.getElementById("paragraph-container").insertAdjacentHTML("afterbegin", paragraph); */




// ############### appendMethods "insertAdjacentText()" ##############
// -------------------------------------------------------------------

/* let text = "Sandeep Malothiya";
document.getElementById("paragraph-container").insertAdjacentText("afterbegin", text); */




// ################ DOM attributes "isId" #################
// --------------------------------------------------------

/* let element = document.getElementById("document-heading").attributes[0].isId;
console.log(element); */




// ################## DOM attributes "name" #####################
// --------------------------------------------------------------

/* let element = document.getElementsByTagName("H1")[0].attributes[0].name;
console.log(element); */




// ################# DOM attributes "value" #################
// ----------------------------------------------------------

/* let element = document.getElementsByTagName("H1")[0].attributes[0].value;
console.log(element); */




// ############## DOM attributes "specified" #################
// -----------------------------------------------------------

/* let element = document.getElementById("heading").attributes[0].specified;
console.log(element); */




// ########################## DOM Clone() Method ##############################
// --------------------------------------------------------------------------

/* let element = document.getElementById("document-paragraph");
let copyElement = element.cloneNode(true);
document.getElementById("paragraph-container").appendChild(copyElement); */




// ###################### DOM Contains() Method ########################
// -------------------------------------------------------------------

/* let parentElement = document.querySelector(".outer-container");
let childElement = document.querySelector(".inner-container");
let find = parentElement.contains(childElement);
console.log(find); */




// ##################### createElement() #########################
// ---------------------------------------------------------------

/* let element = document.createElement("p");
console.log(element); */




// ################## createTextNode() ########################
// -------------------------------------------------------------

/* let element = document.createTextNode("My name is sandeep.");
console.log(element); */



// #################### createComment() ######################
// -----------------------------------------------------------

/* let element = document.createComment("This is a comment.");
console.log(element); */




// ################## activeElement ####################
// -----------------------------------------------------

/* let element = document.activeElement;
console.log(element); */




// ################## anchors #######################
// -------------------------------------------------

/* let element = document.anchors;
console.log(element); */




// ################## baseURI #####################
// ------------------------------------------------

/* let element = document.baseURI;
console.log(element); */




// ##################### body ######################
// -------------------------------------------------

/* let element = document.body;
console.log(element); */




// ################# cookie ####################
// ---------------------------------------------

/* let element = document.cookie;
console.log(element); */




// ################### charset ###################
// -----------------------------------------------

/* let element = document.charset;
console.log(element); */




// ################# characterSet #################
// ------------------------------------------------

/* let element = document.characterSet;
console.log(element); */




// ################ defaultView ####################
// -------------------------------------------------

/* let element = document.defaultView;
console.log(element); */




// ##################### doctype ####################
// -------------------------------------------------

/* let element = document.doctype;
console.log(element); */




// ##################### documentElement ###################
// ---------------------------------------------------------

/* let element = document.documentElement;
console.log(element); */




// ##################### documentURI #######################
// ---------------------------------------------------------

/* let element = document.documentURI;
console.log(element); */




// #################### domain #######################
// ---------------------------------------------------

/* let element = document.domain;
console.log(element); */




// ##################### forms ######################
// --------------------------------------------------

/* let element = document.forms;
console.log(element); */




// ##################### head ########################
// ---------------------------------------------------

/* let element = document.head;
console.log(element); */




// ################### images ######################
// -------------------------------------------------

/* let element = document.images;
console.log(element); */




// ################## inputEncoding #####################
// ------------------------------------------------------

/* let element = document.inputEncoding;
console.log(element); */




// #################### lastModified ########################
// ----------------------------------------------------------

/* let element = document.lastModified;
console.log(element); */




// ################### links #####################
// -----------------------------------------------

/* let element = document.links;
console.log(element); */




// #################### referrer #####################
// ---------------------------------------------------

/* let element = document.referrer;
console.log(element); */




// ##################### scripts ###################
// --------------------------------------------------

/* let element = document.scripts;
console.log(element); */




// ###################### title #######################
// ----------------------------------------------------

/* let element = document.title;
console.log(element); */




// ####################### URL ########################
// ----------------------------------------------------

/* let element = document.URL;
console.log(element); */




// ###################### write ######################
// ----------------------------------------------------

/* document.write("Good Morning, My name is sandeep."); */




// ######################## writeln ###########################
// ------------------------------------------------------------

/* document.writeln("Good Morning, My name is sandeep."); */




// ################### childElementCount ####################
// ----------------------------------------------------------

/* let element = document.getElementById("list").childElementCount;
console.log(element); */




// ################# childNodes #####################
// --------------------------------------------------

/* let element = document.getElementById("list").childNodes;
console.log(element); */




// #################### children #####################
// ---------------------------------------------------

/* let element = document.getElementById("list").children;
console.log(element); */




// ##################### classList ######################
// ------------------------------------------------------

/* let element = document.getElementById("list").classList;
console.log(element); */




// #################### className #######################
// ------------------------------------------------------

/* let element = document.getElementById("list").className;
console.log(element); */




// ###################### firstChild ####################
// ------------------------------------------------------

/* let element = document.getElementById("list").firstChild;
console.log(element); */




// ##################### firstElementChild ######################
// --------------------------------------------------------------

/* let element = document.getElementById("list").firstElementChild;
console.log(element); */




// ##################### id #######################
// ------------------------------------------------

/* let element = document.getElementsByClassName("list")[0].id;
console.log(element); */




// ####################### innerHTML ######################

/* document.getElementById("document-heading").innerHTML = "DOM"; */




// ###################### innerText ######################
// -------------------------------------------------------

/* let element = document.getElementById("document-heading").innerText;
console.log(element); */




// ###################### lastChild #######################
// --------------------------------------------------------

/* let element = document.getElementById("list").lastChild;
console.log(element); */




// ##################### lastElementChild ####################
// -----------------------------------------------------------

/* let element = document.getElementById("list").lastElementChild;
console.log(element); */




// ###################### nextSibling #######################
// ----------------------------------------------------------

/* let element = document.getElementById("first-child").nextSibling;
console.log(element); */




// ########################## nextElementSibling #######################
// ---------------------------------------------------------------------

/* let element = document.getElementById("first-child").nextElementSibling;
console.log(element); */




// ###################### nodeName ######################
// ------------------------------------------------------

/* let element = document.getElementById("first-child").nodeName;
console.log(element); */




// ##################### nodeType ####################
// ---------------------------------------------------

/* let element = document.getElementById("first-child").nodeType;
console.log(element); */




// ##################### nodeValue ######################
// ------------------------------------------------------

/* let element = document.getElementsByTagName("BUTTON")[0].childNodes[0].nodeValue;
console.log(element); */




// ########################## outerHTML ########################
// -------------------------------------------------------------

/* document.getElementById("document-heading").outerHTML = "<h3>Document Object Model</h3>"; */




// ###################### outerText #####################
// -----------------------------------------------------

/* document.getElementById("document-heading").outerText = "Document Object Model"; */




// ##################### parentNode ###################
// ----------------------------------------------------

/* let element = document.getElementById("first-child").parentNode;
console.log(element); */




// ########################## parentElement #####################
// --------------------------------------------------------------

/* let element = document.getElementById("first-child").parentElement;
console.log(element); */




// ######################### previousSibling #######################
// -----------------------------------------------------------------

/* let element = document.getElementById("second-child").previousSibling;
console.log(element); */




// #################### previousElementSibling ######################
// ------------------------------------------------------------------

/* let element = document.getElementById("second-child").previousElementSibling;
console.log(element); */




// ######################### tagName ########################
// ----------------------------------------------------------

/* let element = document.getElementById("document-heading").tagName;
console.log(element); */




// ####################### textContent ######################
// ----------------------------------------------------------

/* let element = document.getElementById("document-heading").textContent;
console.log(element); */




// ######################## title ########################
// -------------------------------------------------------

/* let element = document.getElementById("button").title;
console.log(element); */




// ####################### innerText #####################
// -------------------------------------------------------

/* let element = document.getElementById("document-paragraph").innerText;
console.log(element); */




// ######################### innerHTML ####################
// --------------------------------------------------------

/* let element = document.querySelector(".outer-container").innerHTML;
console.log(element); */




// ###################### getAttribute #####################
// ----------------------------------------------------------

/* let element = document.getElementById("list").getAttribute("id");
console.log(element); */




// #################### getAtrributeNode #######################
// -------------------------------------------------------------

/* let element = document.getElementById("list").getAttributeNode("id");
console.log(element); */




// ####################### attributes #######################
// ----------------------------------------------------------

/* let element = document.getElementById("list").attributes[0];
let elements = document.getElementById("list").attributes[1];

console.log(element);
console.log(elements); */




// ######################## hasAttribute #######################
// -------------------------------------------------------------

/* let element = document.getElementById("text-container");
let find = element.hasAttribute("class");
console.log(find); */




// ###################### hasChildNodes ######################
// -----------------------------------------------------------

/* let element = document.getElementById("list");
let find = element.hasChildNodes();
console.log(find); */




// ###################### Replace Method #######################
// -------------------------------------------------------------

/* let element = document.createElement("li");
let text = document.createTextNode("Tesla");
element.appendChild(text);

let target = document.getElementById("list");
let oldElement = target.children[3];
target.replaceChild(element, oldElement); */




// ##################### Remove Child ##########################
// -------------------------------------------------------------

/* let target = document.getElementById("list");
let element = target.children[3];
target.removeChild(element); */




// ################# getElementByTagName() ###################
// -----------------------------------------------------------

/* let links = document.getElementsByTagName("a");
let link = document.getElementsByTagName("a")[0];

console.log(links);
console.log(link); */




// ###################### getElementByClassName() ####################
// -------------------------------------------------------------------

/* let links = document.getElementsByClassName("child");
let link = document.getElementsByClassName("child")[0];

console.log(links);
console.log(link); */




// ####################### getElementById() ###################
// ------------------------------------------------------------

/* let heading = document.getElementById("document-heading");
console.log(heading); */




// ##################### querySelector() ###################
// --------------------------------------------------------

/* let heading = document.querySelector("#document-heading");
console.log(heading); */




// ###################### querySelectorAll() ###################
// -------------------------------------------------------------

/* let links = document.querySelectorAll(".child");
let link = document.querySelectorAll(".child")[0];

console.log(links);
console.log(link); */




// ##################### innerText ##################
// --------------------------------------------------

/* document.getElementById("document-heading").innerText = "DOM"; */




// #################### innerHTML ###################
// --------------------------------------------------

/* document.getElementById("document-heading").innerHTML = "<h5>Document Object Model</h5>"; */




// ##################### setAttribute ##################
// -----------------------------------------------------

/* document.getElementById("document-heading").setAttribute("class", "darkcyan"); */




// ##################### attributes ####################
// -----------------------------------------------------

/* document.getElementById("document-heading").attributes[1].value = "darkcyan"; */




// ################### removeAttribute ####################
// --------------------------------------------------------

/* document.getElementById("document-heading").removeAttribute("class"); */

