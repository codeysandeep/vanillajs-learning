let fruits = "Apple, Banana, Coconut, Date, Grapes";
let quote = `"The way to get started is to quit talking and begin doing." -Walt Disney`;

console.log(fruits.slice(0, 5));
console.log(fruits.slice(0, -8));
console.log(quote.slice(0, 60));