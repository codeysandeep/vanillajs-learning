// ################ Error Handling "try & catch" ##############
// -----------------------------------------------------------------------

/* try {
    console.log("The sum of given numbers is " + sum());
} catch (error) {
    console.error("There is an error.");
} */




// ########### Error Handling "try-catch & finally" ##############
// ---------------------------------------------------------------

/* try {
    console.log("The sum of given numbers is " + sum());
} catch (error) {
    console.error("There is an error.");
} finally {
    console.log("Report: Your task is successfully executed in your console.");
} */




// ############## Error Handling "try-catch & throw" ################
// ------------------------------------------------------------------

/* function getArea(width, height) {
    if (isNaN(width) || isNaN(height)) {
        throw new Error("Parameter is not a number.");
    }
}
try {
    getArea(10, "A");
} catch (error) {
    console.error("There is an error.");
} */
