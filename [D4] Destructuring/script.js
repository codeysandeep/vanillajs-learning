// ######################### Variables Destructuring #########################
// ---------------------------------------------------------------------------

/* let one, two;
[one, two] = [1, 2];

console.log("One :", one);
console.log("Two :", two); */




// #################################################################################

/* [one, two, three, ...counting] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

console.log("One :", one);
console.log("Two :", two);
console.log("Three :", three);
console.log("Counting :", counting); */




// ########################### Array Destructuring ###############################
// -------------------------------------------------------------------------------

/* ({one, two, three, ...counting} = {one:1, two:2, three:3, four:4, five:5});

console.log("One :", one);
console.log("Two :", two);
console.log("Three :", three);
console.log("Counting :", counting); */




// ################################################################################

/* const fruits = ["Apple", "Banana", "Coconut", "Date", "Elderberry"];
[one, two, three, four, five] = fruits;

console.log("A for", one);
console.log("B for", two);
console.log("C for", three);
console.log("D for", four);
console.log("E for", five); */




// ############################ Object Destructuring ############################
// ------------------------------------------------------------------------------

/* const student = {
    fullname : "Sandeep Malothiya",
    rollno : 1714510036,
    age : 21,
    course : "B.Tech",
    branch : "Computer Science and Engineering"
}

const {fullname, rollno, age, course, branch} = student;

console.log("Full Name :", fullname);
console.log("Roll Number :", rollno);
console.log("Age :", age);
console.log("Course :", course);
console.log("Branch :", branch); */

