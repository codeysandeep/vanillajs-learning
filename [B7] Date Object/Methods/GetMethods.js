const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];
const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
];

let timezone = new Date();

console.log(`Date & Time : ${timezone.toLocaleString()}`);
console.log(`Year : ${timezone.getFullYear()}`);
console.log(`Month : ${months[timezone.getMonth()]}`);
console.log(`Date : ${timezone.getDate()}`);
console.log(`Day : ${days[timezone.getDay()]}`);
console.log(`Time : ${timezone.getTime()}`);
console.log(`Hours : ${timezone.getHours()}`);
console.log(`Minutes : ${timezone.getMinutes()}`);
console.log(`Seconds : ${timezone.getSeconds()}`);
console.log(`Milliseconds : ${timezone.getMilliseconds()}`);