let timezone = new Date();
let birthday = new Date(2000, 8, 16, 8, 30, 0, 0);
let reminder = new Date("February 22, 2021 10:00:00");
let launch = new Date(0);
let today = new Date(1612794915848);

console.log("Date & Time :", timezone.toLocaleString());
console.log("Date of Birthday :", birthday.toLocaleString());
console.log("Date of Exam : ", reminder.toLocaleString());
console.log("Date & Time (Launching) :", launch.toLocaleString());
console.log(`Date & Time (Today) :`, today.toLocaleString());