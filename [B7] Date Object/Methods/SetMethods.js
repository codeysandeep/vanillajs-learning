let timezone = new Date();

console.log(`Time : ${timezone.setTime(1667604905005)}`);
console.log(`Year : ${timezone.setFullYear(2022)}`);
console.log(`Month : ${timezone.setMonth(10)}`);
console.log(`Date : ${timezone.setDate(5)}`);
console.log(`Hours : ${timezone.setHours(5)}`);
console.log(`Minutes : ${timezone.setMinutes(5)}`);
console.log(`Seconds : ${timezone.setSeconds(5)}`);
console.log(`Milliseconds : ${timezone.setMilliseconds(5)}`);