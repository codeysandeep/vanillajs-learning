let age = 101;

if (age < 18) {
    console.log("Sorry, You can not drive!");
} else if (age >= 18 && age <= 100) {
    console.log("Congratulations, You can drive!");
} else {
    console.log("I think, You are not alive for driving!");
}