// ######################## Get Text Data ##########################
// -----------------------------------------------------------------

/* document.getElementById("getTextData").addEventListener("click", () => 
{
    url = "data.txt";
    fetch(url).then((response) => {
        return response.text();
    }).then((data) => {
        console.log(data);
    });
}); */




// ##################### Get JSON Data ########################
// ------------------------------------------------------------

/* document.getElementById("getJsonData").addEventListener("click", () => 
{
    url = "data.json";
    fetch(url).then((response) => {
        return response.json();
    }).then((data) => {
        console.log(`Car Name : ${data.name}`);
        console.log(`Price : ${data.price}`);
        console.log(`Fuel Type : ${data.fuel_type}`);
        console.log(`Transmission : ${data.transmission}`);
    });
}); */




// #################### Fetch Data from API ######################
// ---------------------------------------------------------------

/* document.getElementById("fetchDataFromApi").addEventListener("click", () => 
{
    url = "https://jsonplaceholder.typicode.com/users";
    fetch(url).then((response) => {
        return response.json();
    }).then((data) => {
        for (let i = 0; i < data.length; i++) {
            console.table(data[i]);
        }
    });
}); */




// ####################### POST Data to API ###########################
// --------------------------------------------------------------------

/* document.getElementById("postDataToApi").addEventListener("click", () => 
{
    url = "https://reqres.in/api/users";
    data = '{"name":"Sandeep", "job":"Web Developer"}';
    parameters = { 
        method: "post", 
        headers: { "Content-Type": "application/json", }, 
        body: data, 
    };
    fetch(url, parameters).then((response) => {
        return response.json();
    }).then((data) => {
        console.log(data);
    });
}); */




// ##################### POST Form Data to API ##########################
// ----------------------------------------------------------------------

/* document.getElementById("postFormDataToApi").addEventListener("click", (event) => 
{
    event.preventDefault();
    let name = document.getElementById("name").value;
    let job = document.getElementById("job").value;
    url = "https://reqres.in/api/users";
    data = JSON.stringify({ name: name, job: job });
    parameters = {
        method: "post",
        headers: { "Content-Type": "application/json", },
        body: data,
    };
    fetch(url, parameters).then((response) => {
        return response.json();
    }).then((data) => {
        console.log(data);
    });
}); */

