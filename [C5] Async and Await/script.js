// ######################## Get Text Data ##########################
// -----------------------------------------------------------------

/* document.getElementById("getTextData").addEventListener("click", makerequest);
async function makerequest() {
    const response = await fetch("data.txt");
    const data = await response.text();
    console.log(data);
} */




// ##################### Get JSON Data ########################
// -----------------------------------------------------------

/* document.getElementById("getJsonData").addEventListener("click", makerequest);
async function makerequest() {
    const response = await fetch("data.json");
    const data = await response.json();
    console.log(`Car Name : ${data.name}`);
    console.log(`Price : ${data.price}`);
    console.log(`Fuel Type : ${data.fuel_type}`);
    console.log(`Transmission : ${data.transmission}`);
} */




// #################### Fetch Data from API ######################
// ---------------------------------------------------------------

/* document.getElementById("fetchDataFromApi").addEventListener("click", makeRequest);
async function makeRequest() {
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    const data = await response.json();
    for (let i = 0; i < data.length; i++) {
        console.table(data[i]);
    }
} */




// ####################### POST Data to API ###########################
// -------------------------------------------------------------------

/* document.getElementById("postDataToApi").addEventListener("click", makeRequest);
async function makeRequest() {
    parameters = {
        method: "post",
        headers: { "Content-Type": "application/json", },
        body: '{"name":"Sandeep", "job":"Web Developer"}',
    };
    const response = await fetch("https://reqres.in/api/users", parameters);
    const data = await response.json();
    console.log(data);
} */




// #################### POST Form Data to API ##########################
// ---------------------------------------------------------------------

/* document.getElementById("postFormDataToApi").addEventListener("click", makeRequest);
async function makeRequest(event) {
    event.preventDefault();
    let name = document.getElementById("name").value;
    let job = document.getElementById("job").value;
    parameters = {
        method: "post",
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify({ name: name, job: job }),
    };
    const response = await fetch("https://reqres.in/api/users", parameters);
    const data = await response.json();
    console.log(data);
} */
