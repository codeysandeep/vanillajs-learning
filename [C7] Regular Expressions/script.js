// ###################### Regular Expression "Syntax" #######################
// ___________________________________________________________________________



/* // Regular Expression Literal
// -----------------------------
let expression = /Apple/;
console.log("Regular Expression:", expression);
console.log("Source:", expression.source); */





// ####################### Regular Expression "Functions" ######################
// ______________________________________________________________________________



/* // exec() Functions to Match Expressions with Local Expression 
// --------------------------------------------------------------
let expression = /Apple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);
console.log(result); */



// ###################################################################################



/* // exec() Functions to Match Expressions with Global Expression
// ---------------------------------------------------------------
let expression = /Apple/g;
let string = "Apple is a fruit and Apple is my favourite fruit also.";
let result = expression.exec(string);
console.log(result);
result = expression.exec(string);
console.log(result); */



// ####################################################################################



/* // exec() Functions to Match Expressions with Case-insensitive
// --------------------------------------------------------------
let expression = /APPLE/i;
let string = "Apple is a fruit.";
let result = expression.exec(string);
console.log(result) */



// ########################################################################################



/* // test() - Returns true of false 
// ---------------------------------
let expression = /Apple/;
let string = "Apple is my favourite fruit.";
let result = expression.test(string);
console.log("Is Apple in string? :", result); */



// #########################################################################################



/* // match() - It will return an array of results or null
// -------------------------------------------------------
let expression = /Apple/g;
let string = "Apple is a fruit and Apple is my favourite fruit also.";
let result = string.match(expression);
console.log(result); */



// #############################################################################################



/* // search() - Returns index of first match else -1
// --------------------------------------------------
let expression = /Apple/;
let string = "Apple is a fruit and Apple is my favourite fruit also.";
let result = string.search(expression);
console.log('Index of "Apple" :', result); */



// ###########################################################################################



/* // replace() - Returns new string with all the replacements
// -----------------------------------------------------------
let expression = /Apple/g;
let string = "Apple is a fruit and Apple is my favourite fruit also.";
let result = string.replace(expression, "Orange");
console.log(`Codey Sandeep : "${result}"`); */





// ############################# Meta Characters ##############################
// ____________________________________________________________________________



/* // ^expression - Matches any string with expression at the beginning of it
// --------------------------------------------------------------------------
let expression = /^Apple/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches at the beginning of the string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match at the beginning of the string "${string}"`);
} */



// ##########################################################################################################################



/* // expression$ - Matches any string with expression at the end of it.
// ---------------------------------------------------------------------
let expression = /fruit$/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches at the end of the string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match at the end of the string "${string}"`);
} */



// ###########################################################################################################################



/* // . metacharacter is used to find a single character, except newline or other line terminators
// -----------------------------------------------------------------------------------------------
let expression = /A.ple/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



// // expression* - Matches any string that contains zero or more occurrences of expression.
// // ---------------------------------------------------------------------------------------
// let expression = /Ap*/;
// let string = "Apple is a fruit";
// let result = expression.exec(string);

// if (expression.test(string)) {
//     console.log(`The expression source "${expression.source}" matches with string "${string}"`);
// } else {
//     console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
// }



// #################################################################################################################



/* // expression? - Matches any string that contains zero or one occurences of expression
// ---------------------------------------------------------------------------------------
let expression = /Ap?pl?/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ####################################################################################################################



// // \* - Matches exect * in the string
// // ----------------------------------
// let expression = /55\*/;
// let string = "Universe Boss scored 55* yesterday";
// let result = expression.exec(string);

// if (expression.test(string)) {
//     console.log(`The expression source "${expression.source}" matches with string "${string}"`);
// } else {
//     console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
// }





// #################################### Quantifiers ###########################################
// _____________________________________________________________________________________________



/* // n{X} - Matches any string that contains a sequence of X n's
// ---------------------------------------------------------------
let expression = /Ap{2}le/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ############################################################################################################



/* // n{X,Y} - Matches any string that contains a sequence of X to Y n's
// ---------------------------------------------------------------------
let expression = /Ap{0,2}le/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ###############################################################################################################



/* // Groupings - We use paranthesis for groupings ()
// --------------------------------------------------
let expression = /(b[A-Z]){2}([0-9][a-zA-Z]){2}/;
let string = "My password is bZbC8D0c";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */





// ################################# Character Sets #######################################
// ________________________________________________________________________________________



/* // [a-z] - Find any character from the alphabet
// -----------------------------------------------
let expression = /A[a-z]ple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ############################################################################################################



/* // [A-Z] - Find any character from the Alphabet
// ------------------------------------------------
let expression = /[A-Z]pple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



/* // [0-9] - Find any character from the 0-9
// ------------------------------------------
let expression = /[0-9]510/;
let string = "0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ################################################################################################################



/* // [ABC] - Find any character between the brackets
// --------------------------------------------------
let expression = /[ABC]pple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ####################################################################################################################



/* // [^ABC] - Find any character NOT between the brackets
// -------------------------------------------------------
let expression = /[^ABC]pple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##################################################################################################################



/* // [^0-9] - Find any character NOT between the brackets
// -------------------------------------------------------
let expression = /[^0-9]510/;
let string = "0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ###################################################################################################################



/* // [a-zA-Z] - Find any character from the alphabet
// --------------------------------------------------
let expression = /[a-zA-Z]pple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */





// ################################### Shorthand Character Classes #############################
// _____________________________________________________________________________________________



/* // \w - Find a word character
// -----------------------------
let expression = /\wpple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##########################################################################################################



/* // \w+ - Find one or more word character
// ----------------------------------------
let expression = /\w+ple/;
let string = "Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ###########################################################################################################



/* // \W - Find a non-word character
// ---------------------------------
let expression = /\WApple/;
let string = "$Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



/* // \W+ - Find one or more non-word character
// --------------------------------------------
let expression = /\W+Apple/;
let string = "$%Apple is a fruit.";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ############################################################################################################



/* // \d - Find a digit
// --------------------
let expression = /\d510/;
let string = "0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ###########################################################################################################



/* // \d+ - Find one or more digit
// -------------------------------
let expression = /\d+10/;
let string = "0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##########################################################################################################



/* // \D - Find a non digit
// ------------------------
let expression = /\D0510/;
let string = "L0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// #########################################################################################################



/* // \D+ - Find one or more non digit
// ---------------------------------
let expression = /\D+0510/;
let string = "user0510";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// #############################################################################################################



/* // \s - Find a whitespace character
// -----------------------------------
let expression = /\snumber/;
let string = "Roll number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ################################################################################################################



/* // \s+ - Find one or more whitespace character
// ----------------------------------------------
let expression = /\s+number/;
let string = "Roll  number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



/* // \S - Find a non whitespace character
// ---------------------------------------
let expression = /\Snumber/;
let string = "Roll_number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ################################################################################################################



/* // \S+ Find one or more white space character
// ---------------------------------------------
let expression = /\S+number/;
let string = "Roll_number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##########################################################################################################



/* // \b - Find a match at the beginning/ end of a word
// ----------------------------------------------------
let expression = /number\b/;
let string = "Roll number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



/* // \B - Find a match, but not at the beginning/end of a word
// -------------------------------------------------------------
let expression = /\Bnumber/;
let string = "Roll_number : 1714516365";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// #############################################################################################################



/* // ?=n - Matches any string that is followed by a specific string n
// --------------------------------------------------------------------
let expression = /A(?=p)/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */



// ##############################################################################################################



/* // ?!n - Matches any string that is not followed by a specific string n
// ------------------------------------------------------------------------
let expression = /A(?!p)/;
let string = "Apple is a fruit";
let result = expression.exec(string);

if (expression.test(string)) {
    console.log(`The expression source "${expression.source}" matches with string "${string}"`);
} else {
    console.log(`The expression source "${expression.source}" doesn't match with string "${string}"`);
} */

