let fruits = [
    "Apple",
    "Banana",
    "Coconut",
    "Date",
    "Emblica",
    "Feijoa",
    "Grapes",
    "Highbush Blueberry",
    "Ice Cream Bean",
    "Jatoba",
];

fruits.forEach(function(fruit, index) {
    console.log(index + 1, fruit);
});