const dictionary = {
    A: "Apple",
    B: "Banana",
    C: "Coconut",
    D: "Dog",
    E: "Elephant",
    F: "Fish",
    G: "Grapes",
    H: "Hen",
    I: "Ink",
    J: "Jungle",
    K: "King",
    L: "Lion",
    M: "Mango",
    N: "Nose",
    O: "Orange",
    P: "Papaya",
    Q: "Queen",
    R: "Rose",
    S: "Sun",
    T: "Tree",
    U: "Umbrella",
    V: "Vampire",
    W: "Watch",
    X: "X-Ray",
    Y: "Yellow",
    Z: "Zebra",
};

for (let alphabet in dictionary) {
    console.log(`${alphabet} for ${dictionary[alphabet]}`);
}