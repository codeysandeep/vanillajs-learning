let fruits = [
    "Apple",
    "Banana",
    "Coconut",
    "Date",
    "Emblica",
    "Feijoa",
    "Grapes",
    "Highbush Blueberry",
    "Ice Cream Bean",
    "Jatoba",
];

for (let fruit of fruits) {
    console.log(fruit);
}