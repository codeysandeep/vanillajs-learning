// ################### Initializing an empty set ###################
// -----------------------------------------------------------------

const createdSet = new Set();




// ################## Adding Values to the set #####################
// -----------------------------------------------------------------

createdSet.add("JavaScript");
createdSet.add("9.6");
createdSet.add(2021);
createdSet.add(true);
createdSet.add(false);




// ################### Use a constructor to initialize the set #####################
// ---------------------------------------------------------------------------------

let otherSet = new Set(["JavaScript", "9.6", 2021, {true:true, false:false}]);




// ####################### Get the size of the set ######################
// ---------------------------------------------------------------------

/* console.log("Size of the Set :", createdSet.size); */




// ################# Check whether set has the value or not #################
// --------------------------------------------------------------------------

/* console.log("Does set contains 2021? :", createdSet.has(2021)); */




// ################### Delete an element from the set #####################
// ------------------------------------------------------------------------

/* console.log("Before deleting 'false' :", createdSet);
createdSet.delete(false);
console.log("After deleting 'false' :", createdSet); */




// ######################## Iterating set using for of loop ########################
// ---------------------------------------------------------------------------------

/* for(let item of createdSet){
    console.log(item);
} */




// ##################### Iterating set using forEach Loop ########################
// -------------------------------------------------------------------------------

/* createdSet.forEach((item) => {
    console.log(item);
}) */




// ######################## Convertion of Set into Array ########################
// ------------------------------------------------------------------------------

/* let setArray = Array.from(createdSet);
console.log(setArray); */

