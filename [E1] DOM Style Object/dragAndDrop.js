const imageBox = document.querySelector('.image-box');
const whiteBoxes = document.getElementsByClassName('white-box');


imageBox.addEventListener('dragstart', (event) => {
    event.target.className += ' hold';
    setTimeout(() => {
        event.target.className = 'hide';
    }, 0);
})

imageBox.addEventListener('dragend', (event) => {
    event.target.className = 'image-box';
})


for (whiteBox of whiteBoxes) {

    whiteBox.addEventListener('dragover', (event) => {
        event.preventDefault();
    })

    whiteBox.addEventListener('dragenter', (event) => {
        event.target.className += ' dashed';
    })

    whiteBox.addEventListener('dragleave', (event) => {
        event.target.className = 'white-box';
    })

    whiteBox.addEventListener('drop', (event) => {
        event.target.append(imageBox);
    })
}

