// "=" Operator
let A = 5;
console.log("5 =", A);

// "+=" Operator
let B = 5;
console.log("5 += 5 =", (B += 5));

// "-=" Operator
let C = 5;
console.log("5 -= 5 =", (C -= 5));

// "*=" Operator
let D = 5;
console.log("5 *= 5 =", (D *= 5));

// "/=" Operator
let E = 5;
console.log("5 /= 5 =", (E /= 5));

// "%=" Operator
let F = 5;
console.log("5 %= 2 =", (F %= 2));

// "<<=" Operator
let G = 5;
console.log("5 <<= 2 =", (G <<= 2));

// ">>=" Operator
let H = 5;
console.log("5 >>= 2 =", (H >>= 2));

// ">>>=" Operator
let I = 5;
console.log("5 >>>= 2 =", (I >>>= 2));

// "&=" Operator
let J = 5;
console.log("5 &= 5 =", (J &= 5));

// "^=" Operator
let K = 5;
console.log("5 ^= 5 =", (K ^= 5));

// "|=" Operator
let L = 5;
console.log("5 |= 5 =", (L |= 5));

// "**=" Operator
let M = 5;
console.log("5 **= 2 =", (M **= 2));