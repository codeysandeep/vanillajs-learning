let A = 5;
let B = 1;

// AND Operator
console.log("5 & 1 =", A & B);

// OR Operator
console.log("5 | 1 =", A | B);

// NOT Operator
console.log("~5 =", ~A);

// XOR Operator
console.log("5 ^ 1 =", A ^ B);

// Zero fill left shift Operator
console.log("5 << 1 =", A << B);

// Signed right shift Operator
console.log("5 >> 1 =", A >> B);

// Zero fill right shift Operator
console.log("5 >>> 1 =", A >>> B);