let A = 10;
let B = 5;

// Equal to
console.log("10 == 5 :", A == B);

// Equal Value and Equal Type
console.log("10 === 5 :", A === B);

// Not equal
console.log("10 != 5 :", A != B);

// Not equal value or not equal type
console.log("10 !== 5 :", A !== B);

// Greater than
console.log("10 > 5 :", A > B);

// Less than
console.log("10 < 5 :", A < B);

// Greater than or equal to
console.log("10 >= 5 :", A >= B);

// Less than or equal to
console.log("10 <= 5 :", A <= B);