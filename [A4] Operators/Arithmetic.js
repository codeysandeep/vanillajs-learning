let A = 5;
let B = 2;

// "+" Addition Operator
console.log("5 + 2 =", A + B);

// "-" Subtraction Operator
console.log("5 - 2 =", A - B);

// "*" Multiplication Operator
console.log("5 * 2 =", A * B);

// "/" Division Operator
console.log("5 / 2 =", A / B);

// "**" Exponentiation Operator
console.log("5 ** 2 =", A ** B);

// "%" Modulas Operator
console.log("5 % 2 =", A % B);

// "++" Increment Operators
console.log("5++ =", A++);
console.log("++5 =", ++A);

// "--" Decrement Operators
console.log("2-- =", B--);
console.log("--2 =", --B);