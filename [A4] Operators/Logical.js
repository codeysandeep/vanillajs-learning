let A = 10;
let B = 10;
let C = 5;

// AND
console.log("10 = 10 && 10 > 5 :", A == B && B > C);

// OR
console.log("10 != 10 || 10 < 5 :", A != B || B == C);

// NOT
console.log("!(10 != 10) :", !(A != B));