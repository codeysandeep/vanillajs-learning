let hundred = 100;
let fifty = -50;
let infinite = Infinity;

console.log("100 is finite? :", Number.isFinite(hundred));
console.log("-50 is finite? :", Number.isFinite(fifty));
console.log("Infinity is finite? :", Number.isFinite(infinite));