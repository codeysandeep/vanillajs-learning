let age = 10;
let version = 9.91;
let isAlive = true;

console.log("10 is Integer? :", Number.isInteger(age));
console.log("9.91 is Integer? :", Number.isInteger(version));
console.log("true is Integer? :", Number.isInteger(isAlive));