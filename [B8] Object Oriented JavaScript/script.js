// ####################### Object Literals ########################
// ----------------------------------------------------------------

/* let car = {
    name: 'Kia Seltos',
    topSpeed: '170km/h',
    isRunning: function(){
        console.log("Car is running...");
    }
};

console.log(`Car Name : ${car.name}`);
console.log(`Top Speed : ${car.topSpeed}`);
car.isRunning(); */




// ######################## Constructor ###########################
// ----------------------------------------------------------------

/* function Car(givenName, givenSpeed){
    this.name = givenName;
    this.topSpeed = givenSpeed;
    this.summary = function(){
        console.log(`Car Name : ${this.name} | Top Speed : ${this.topSpeed}`);
    }
}

firstCar = new Car("Kia Seltos", "170km/h");
secondCar = new Car("Hennessey Venom", "301mp/h");

firstCar.summary();
secondCar.summary(); */




// ############################## Class #############################
// ------------------------------------------------------------------

/* class Employee {
    constructor(name, salary, experience) {
        this.name = name;
        this.salary = salary;
        this.experience = experience;
    };
    slogan() {
        return `"I am ${this.name} and this company is best."`;
    };
    joiningYear() {
        return `My joining year was ${2020 - this.experience}.`;
    };
    static friends(college, school){
        return `My total friends are ${college + school}.`;
    };
};

manager = new Employee("Sandeep Malothiya", 295000, 25);

console.log(manager);
console.log(manager.slogan());
console.log(manager.joiningYear());
console.log(Employee.friends(454586, 963214)); */




// ############################ Inheritance ########################
// -----------------------------------------------------------------

/* class Employee {                                                                              
    constructor(name, salary, experience) {
        this.name = name;
        this.salary = salary;
        this.experience = experience;
    };
    slogan() {
        return `"I am ${this.name} and this company is best."`;
    };
    joiningYear() {
        return `My joining year was ${2020 - this.experience}.`;
    };
    static friends(college, school){
        return `My total friends are ${college + school}.`;
    };
};

class Intern extends Employee {                                           
    constructor(name, salary, experience, language, github){
        super(name, salary, experience);
        this.language = language;
        this.github = github
    };
    favouriteLanguage() {
        if (this.language == "python"){
            return 'My favourite language is "Python".';
        }
        else{
            return 'My favourite language is "JavaScript".';
        }
    };
    static students(home, tution){
        return `My total students are ${home + tution}.`;
    };
};

sandeep = new Intern("Sandeep Malothiya", 315000, 2, "JavaScript", "github/sandeepmalothiya");

console.log(sandeep.favouriteLanguage());
console.log(Intern.students(454561, 9845454)); */




// ########################## Prototype ############################
// -----------------------------------------------------------------

/* let mobile = function(company, model, price, modelNumber) 
{
    this.company = company;          // Prototype Instances
    this.model = model;
    this.price = price;
    this.modelNumber = modelNumber;
};

mobile.prototype.color = "Black";    // Prototype Members
mobile.prototype.version = "4.3.2";

let samsung = new mobile("Samsung", "Galaxy A10", 8999, 11457856322);

console.log(`Product : ${samsung.company} ${samsung.model}`);
console.log(`Price : ${samsung.price}`);
console.log(`Model Number : ${samsung.modelNumber}`);
console.log(`Color : ${samsung.color}`);
console.log(`Version : ${samsung.version}`); */




// ##################### Prototype Inheritance ######################
// ------------------------------------------------------------------

/* let Employee = function(name, salary, experience) 
{
    this.name = name;                   // Prototype Instances
    this.salary = salary;            
    this.experience = experience;    
};

Employee.prototype.slogan = function()  // Prototype Member
{
    return `"This company is the best." Regards, ${this.name}`;
};

let Intern = function(name, salary, experience, language) {
    Employee.call(this, name, salary, experience);
    this.language = language;
};

Intern.prototype = Object.create(Employee.prototype);
Intern.prototype.constructor = Intern;

let rohit = new Intern("Rohit Kushwaha", 25000, "1 year", "Python");

console.log(`Employee Name : ${rohit.name}`);
console.log(`Salary : ${rohit.salary}`);
console.log(`Experience : ${rohit.experience}`);
console.log(`Language : ${rohit.language}`);
console.log(rohit.slogan()); */
