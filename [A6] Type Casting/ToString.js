// Number to String
let age = String(18);
console.log("Data :", age, "| Data Type :", typeof age);

// Boolean to String
let isAlive = String(true);
console.log("Data :", isAlive, "| Data Type :", typeof isAlive);

// Date to String
let date = String(new Date());
console.log("Data :", date, "| Data Type :", typeof date);

// Null to String
let income = String(null);
console.log("Data :", income, "| Data Type :", typeof income);

// Undefined to String
let bugs = String(undefined);
console.log("Data :", bugs, "| Data Type :", typeof bugs);