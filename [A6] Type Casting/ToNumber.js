// String to Number
let pi = Number("3.14");
console.log("Data :", pi, "| Data Type :", typeof pi);

// Boolean to Number
let isAlive = Number(true);
console.log("Data :", isAlive, "| Data Type :", typeof isAlive);

// Date to Number
let date = Number(new Date());
console.log("Data :", date, "| Data Type :", typeof date);

// Null to Number
let income = Number(null);
console.log("Data :", income, "| Data Type :", typeof income);

// Undefined to Number
let bugs = Number(undefined);
console.log("Data :", bugs, "| Data Type :", typeof bugs);