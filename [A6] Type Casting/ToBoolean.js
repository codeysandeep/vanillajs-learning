// String to Boolean
let fruit = Boolean("Apple");
console.log("Data :", fruit, "| Data Type :", typeof fruit);

// Number To Boolean
let age = Boolean(18);
console.log("Data :", age, "| Data Type :", typeof age);

// Date to Boolean
let date = Boolean(new Date());
console.log("Data :", date, "| Data Type :", typeof date);

// Null to Boolean
let income = Boolean(null);
console.log("Data :", income, "| Data Type :", typeof income);

// Undefined to Boolean
let bugs = Boolean(undefined);
console.log("Data :", bugs, "| Data Type :", typeof bugs);