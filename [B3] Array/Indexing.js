let fruits = ["Apple", "Banana", "Coconut", "Date", "Grapes"];

console.log("A for", fruits[0]);
console.log("B for", fruits[1]);
console.log("C for", fruits[2]);
console.log("D for", fruits[3]);
console.log("G for", fruits[fruits.length - 1]);