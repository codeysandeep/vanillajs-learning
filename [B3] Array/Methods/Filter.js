let prices = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

const filterPrices = prices.filter((price) => {
    return price > 400;
});

console.log("All Prices Upto 400 :", filterPrices);