let fruits = ["Apple", "Banana", "Coconut", "Apple", "Grapes", "Banana"];

console.log("Last Index of Apple :", fruits.lastIndexOf("Apple"));
console.log("Last Index of Banana :", fruits.lastIndexOf("Banana"));
console.log("Last Index of Banana :", fruits.lastIndexOf("Banana", 4));