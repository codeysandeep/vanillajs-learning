const prices = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

let sum = prices.reduce((accumulator, price, index, array) => {
    return (accumulator += price);
}, 5000);

console.log("Total Amount =", sum);