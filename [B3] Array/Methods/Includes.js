let fruits = ["Apple", "Banana", "Coconut", "Date", "Grapes"];

let appleVerification = fruits.includes("Apple");
let papayaVerification = fruits.includes("Papaya");

console.log("Does fruits include 'Apple'? :", appleVerification);
console.log("Does fruits include 'Papaya'? :", papayaVerification);