var fullname = "Codey Sandeep";
var age = 22;
var single = true;

function Introduction() {
    console.log("My Introduction from Inside the function : ");
    console.log("Name : ", fullname);
    console.log("Age : ", age);
    console.log("Single : ", single);
}

Introduction();

console.log("My Introduction from Outside the function : ");
console.log("Name : ", fullname);
console.log("Age : ", age);
console.log("Single : ", single);