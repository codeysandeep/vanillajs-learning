let fullname = "Codey Sandeep";
let age = 22;
let single = true;

function Introduction() {
    let fullname = "Sandeep Malothiya";
    let age = 21;
    let single = false;
    console.log("My Introduction from Inside the function : ");
    console.log("Name : ", fullname);
    console.log("Age : ", age);
    console.log("Single : ", single);
}

Introduction();

console.log("My Introduction from Outside the function : ");
console.log("Name : ", fullname);
console.log("Age : ", age);
console.log("Single : ", single);