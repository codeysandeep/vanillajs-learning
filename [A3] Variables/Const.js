const fullname = "Codey Sandeep";
const age = 22;
const single = true;

function Introduction() {
    const fullname = "Sandeep Malothiya";
    const age = 21;
    const single = false;
    console.log("My Introduction from Inside the function : ");
    console.log("Name : ", fullname);
    console.log("Age : ", age);
    console.log("Single : ", single);
}

Introduction();

console.log("My Introduction from Outside the function : ");
console.log("Name : ", fullname);
console.log("Age : ", age);
console.log("Single : ", single);