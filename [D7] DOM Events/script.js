// ######################## onblur() #############################
// --------------------------------------------------------------

function firstFunction() {
    let element = document.getElementById("name");
    element.value = element.value.toUpperCase();
}



// ###################### onchange() ########################
// ----------------------------------------------------------

function secondFunction() {
    let element = document.getElementById("fruit").value;
    console.log(`Your favourite fruit is ${element}.`);
};




// ################### onclick() ######################
// ----------------------------------------------------

function thirdFunction() {
    console.log("Please Don't Click Me!");
};




// ################## onCopy() ###################
// -----------------------------------------------

function fourthFunction() {
    console.log("You have successfully copied text from text area.");
};




// ##################### onCut() ########################
// ------------------------------------------------------

function fifthFunction() {
    console.log("You have successfully cut this text from text content.");
};




// ##################### ondblclick() #######################
// ----------------------------------------------------------

function sixthFunction() {
    console.log("Please Don't Double Click Me!");
};




// ####################### onfocus() #######################
// ---------------------------------------------------------

function seventhFunction() {
    document.getElementById("father-name").style.background = "yellow";
};




// ####################### oninput() ######################
// --------------------------------------------------------

function eighthFunction() {
    let element = document.getElementById("mother-name").value;
    console.log(element);
};




// ##################### onmouseover() #####################
// ---------------------------------------------------------

function ninthFunction() {
    document.getElementById("heading").style.color = "red";
};




// ####################### onmousedown()  #########################
// ----------------------------------------------------------------

function tenthFunction() {
    document.getElementById("paragraph").style.color = "lightgreen";
};




// ###################### onmouseenter() #####################
// -----------------------------------------------------------

function eleventhFunction() {
    document.getElementById("paragraph").style.color = "green";
}




// ##################### onmouseleave() #######################
// ------------------------------------------------------------

function twelvethFunction() {
    document.getElementById("paragraph").style.color = "black";
}




// ################### onmouseout() ####################
// -----------------------------------------------------

function thirteenthFunction() {
    document.getElementById("heading").style.color = "black";
};




// ####################### onpaste() #####################
// -------------------------------------------------------

function fourteenthFunction() {
    console.log("You successfully paste text into text area.");
};




// ################### onsearch() #####################
// ----------------------------------------------------

function fifteenthFunction() {
    let element = document.getElementById("search-content").value;
    console.log(`You are searching for ${element}.`);
};




// ##################### onselect() ######################
// -------------------------------------------------------

function sixteenthFunction() {
    console.log("You select some content from content area!");
};
