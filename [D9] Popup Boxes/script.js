// #################### alert() ###################
// ------------------------------------------------

function alertFunction() {
    alert("Welcome to Our Site!");
};




// ###################### confirm() ####################
// -----------------------------------------------------

function confirmFunction() {
    confirm("I know, you are learning JavaScript!");
};




// #################### prompt() #####################
// ---------------------------------------------------

function promptFunction() {
    prompt("What is your name?");
};



