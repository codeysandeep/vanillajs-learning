// ############### Creating Symbols - Symbols are the unique primitives ##############
// -----------------------------------------------------------------------------------

const firstSymbol = Symbol('My Indentifier');
const secondSymbol = Symbol('My Identifier');




// ################### Check whether the symbols are equal or not #################
// --------------------------------------------------------------------------------

/* console.log("Are the symbols equal? :", firstSymbol===secondSymbol); */




// ######################### Creating Keys using Symbols #########################
// -------------------------------------------------------------------------------

fruits = {};
const firstKey = Symbol("Apple is a fruit");
const secondKey = Symbol("Banana is a fruit");
fruits[firstKey] = "Apple";
fruits[secondKey] = "Banana";
fruits["thirdKey"] = "Coconut";
fruits["fourthKey"] = "Date";




// ####################### Symbols are ignored in for in loop ##########################
// -------------------------------------------------------------------------------------

/* for(fruit in fruits){
    console.log(`Key : ${fruit} | Value : ${fruits[fruit]}`);
} */




// ######################## Convert Object into JSON ###########################
// -----------------------------------------------------------------------------

/* let fruitsJson = JSON.stringify(fruits);
console.log(fruitsJson); */

