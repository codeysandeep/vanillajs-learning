// #################### Creating a Map Object ##################
// -------------------------------------------------------------

const createdMap = new Map();




// ################### Creating Keys for Map Object ##################
// -------------------------------------------------------------------

const firstKey = "string", secondKey = {}, thirdKey = function() {};




// ################## Setting Key Values in Map Object ###################
// -----------------------------------------------------------------------

createdMap.set(firstKey, "This is a string");
createdMap.set(secondKey, "This is a blank Object");
createdMap.set(thirdKey, "This is an empty function");




// ################# Getting the values from the Map Object #################
// --------------------------------------------------------------------------

/* let firstValue = createdMap.get(firstKey);
let secondValue = createdMap.get(secondKey);
let thirdValue = createdMap.get(thirdKey);
console.log(`First Value : "${firstValue}"`);
console.log(`Second Value : "${secondValue}"`);
console.log(`Third Value : "${thirdValue}"`); */




// ###################### Get the size of the Map ########################
// -----------------------------------------------------------------------

/* console.log("Size of Map Object : ", createdMap.size); */




// ################# For of Loop to get Keys and Values ###################
// ------------------------------------------------------------------------

/* for (let [key, value] of createdMap) {
    console.log("Key :", key);
    console.log("Value :", value);
} */




// ############################ Get only keys #############################
// -----------------------------------------------------------------------

/* for (let key of createdMap.keys()){
    console.log("Key :", key);
} */




// ########################### Get only Values ###############################
// ---------------------------------------------------------------------------

/* for (let value of createdMap.values()){
    console.log(`Value : "${value}"`);
} */




// ######################## Converting Map Object to Array #######################
// ------------------------------------------------------------------------------

/* let mapArray = Array.from(createdMap);
console.log(mapArray); */




// ####################### Convert Map Object Keys to Array ###########################
// ------------------------------------------------------------------------------------

/* let keysArray = Array.from(createdMap.keys())
console.log(keysArray); */




// #################### Converting Map Object Values to Array ######################
// ---------------------------------------------------------------------------------

/* let valuesArray = Array.from(createdMap.values());
console.log(valuesArray); */

