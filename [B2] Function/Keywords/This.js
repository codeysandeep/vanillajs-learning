const student = {
    firstName: "Sandeep",
    lastName: "Malothiya",
    age: 21,
    course: "B.Tech",

    fullName() {
        return `${this.firstName} ${this.lastName}`;
    },
};

console.log(`Name : ${student.fullName()}`);
console.log(`Age : ${student.age}`);
console.log(`Course : ${student.course}`);