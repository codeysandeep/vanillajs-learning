function greeting(name, greet = "Morning") {
    return `Good ${greet}, my name is ${name}.`;
}

console.log(greeting("Sandeep"));
console.log(greeting("Shivam", "Evening"));