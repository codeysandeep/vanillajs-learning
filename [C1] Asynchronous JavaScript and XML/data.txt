"September Eleventh Two Thousand and One
The Big Apple awoke, a new day had begun.
The Statue of Liberty was standing so proud
As God and Freedom were singing out loud."

Writer - Codey Sandeep