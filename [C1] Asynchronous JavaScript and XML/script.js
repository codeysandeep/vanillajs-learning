// ###################### Get Text Data #############################
// -----------------------------------------------------------------

/* document.getElementById("getTextData").addEventListener("click", () => 
{
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "data.txt", true);
    xhr.onload = function () {
    if (xhr.status === 200) { 
        console.log(xhr.responseText);
     } else { 
        console.log("No response found...");
    }
  }
  xhr.send();
}); */




// ######################## Get JSON Data ##############################
// --------------------------------------------------------------------

/* document.getElementById("getJsonData").addEventListener("click", () => 
{
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "data.json", true);
    xhr.responseType = "json";
    xhr.onload = function () {
    if (xhr.status === 200) {
      console.log(`Car Name : ${xhr.response.name}`);
      console.log(`Price : ${xhr.response.price}`);
      console.log(`Fuel Type : ${xhr.response.fuel_type}`);
      console.log(`Transmission : ${xhr.response.transmission}`);
    } else {
      console.log("No response found...");
    }
  }
  xhr.send();
}); */




// ########################## Get XML Data ##########################
// ------------------------------------------------------------------

/* document.getElementById("getXmlData").addEventListener("click", () => 
{
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "data.xml", true);
    xhr.responseType = "document";
    xhr.onload = function() {
        if(xhr.status === 200) {
            let text = xhr.response.getElementsByTagName("mahindra");
            console.log(`Car Name : ${text[0].getElementsByTagName("name")[0].childNodes[0].nodeValue}`);
            console.log(`Price : ${text[0].getElementsByTagName("price")[0].childNodes[0].nodeValue}`);
            console.log(`Gears : ${text[0].getElementsByTagName("gears")[0].childNodes[0].nodeValue}`);
            console.log(`Speed : ${text[0].getElementsByTagName("speed")[0].childNodes[0].nodeValue}`); 
        } else {
            console.log("No response found...");
        }
    }
    xhr.send();
}); */




// ####################### Fetch Data from API #######################
// -------------------------------------------------------------------

/* document.getElementById("fetchDataFromApi").addEventListener("click", () => 
{
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "https://jsonplaceholder.typicode.com/users", true);
    xhr.responseType = "json";
    xhr.onload = function() {
      if(xhr.status === 200){
          let text = xhr.response;
          for(let i=0; i<text.length; i++){
              console.table(text[i]);
          }
      } else {
          console.log("No response found...");
      }
  }
  xhr.send();
}); */




// ####################### Post Data to API ########################
// -----------------------------------------------------------------

/* document.getElementById("postDataToApi").addEventListener("click", () => 
{
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://dummy.restapiexample.com/api/v1/create", true);
    xhr.getResponseHeader('Content-type', 'application/json');
    xhr.onload = function() {
        if(xhr.status === 200){
            console.log(xhr.response);
        } else {
            console.log("No response found...");
        }
    }
    data = `{"name":"Sandeep Malothiya","salary":"684000","age":"21"}`;
    xhr.send(data);
}); */

